// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "SonicCharacter.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROTOSONIC_SonicCharacter_generated_h
#error "SonicCharacter.generated.h already included, missing '#pragma once' in SonicCharacter.h"
#endif
#define PROTOSONIC_SonicCharacter_generated_h

#define FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicCharacter_h_15_SPARSE_DATA
#define FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicCharacter_h_15_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicCharacter_h_15_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS
#define FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicCharacter_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicCharacter_h_15_ACCESSORS
#define FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicCharacter_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASonicCharacter(); \
	friend struct Z_Construct_UClass_ASonicCharacter_Statics; \
public: \
	DECLARE_CLASS(ASonicCharacter, ANinjaCharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ProtoSonic"), NO_API) \
	DECLARE_SERIALIZER(ASonicCharacter)


#define FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicCharacter_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASonicCharacter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASonicCharacter(ASonicCharacter&&); \
	NO_API ASonicCharacter(const ASonicCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASonicCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASonicCharacter); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASonicCharacter) \
	NO_API virtual ~ASonicCharacter();


#define FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicCharacter_h_12_PROLOG
#define FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicCharacter_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicCharacter_h_15_SPARSE_DATA \
	FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicCharacter_h_15_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicCharacter_h_15_EDITOR_ONLY_SPARSE_DATA_PROPERTY_ACCESSORS \
	FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicCharacter_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicCharacter_h_15_ACCESSORS \
	FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicCharacter_h_15_INCLASS_NO_PURE_DECLS \
	FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicCharacter_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PROTOSONIC_API UClass* StaticClass<class ASonicCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
