// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ProtoSonic/SonicMovementComponent.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSonicMovementComponent() {}
// Cross Module References
	NINJACHARACTER_API UClass* Z_Construct_UClass_UNinjaCharacterMovementComponent();
	PROTOSONIC_API UClass* Z_Construct_UClass_USonicMovementComponent();
	PROTOSONIC_API UClass* Z_Construct_UClass_USonicMovementComponent_NoRegister();
	UPackage* Z_Construct_UPackage__Script_ProtoSonic();
// End Cross Module References
	void USonicMovementComponent::StaticRegisterNativesUSonicMovementComponent()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(USonicMovementComponent);
	UClass* Z_Construct_UClass_USonicMovementComponent_NoRegister()
	{
		return USonicMovementComponent::StaticClass();
	}
	struct Z_Construct_UClass_USonicMovementComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USonicMovementComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNinjaCharacterMovementComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_ProtoSonic,
	};
	static_assert(UE_ARRAY_COUNT(Z_Construct_UClass_USonicMovementComponent_Statics::DependentSingletons) < 16);
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USonicMovementComponent_Statics::Class_MetaDataParams[] = {
#if !UE_BUILD_SHIPPING
		{ "Comment", "/**\n * \n */" },
#endif
		{ "IncludePath", "SonicMovementComponent.h" },
		{ "ModuleRelativePath", "SonicMovementComponent.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USonicMovementComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USonicMovementComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_USonicMovementComponent_Statics::ClassParams = {
		&USonicMovementComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B000A4u,
		METADATA_PARAMS(UE_ARRAY_COUNT(Z_Construct_UClass_USonicMovementComponent_Statics::Class_MetaDataParams), Z_Construct_UClass_USonicMovementComponent_Statics::Class_MetaDataParams)
	};
	UClass* Z_Construct_UClass_USonicMovementComponent()
	{
		if (!Z_Registration_Info_UClass_USonicMovementComponent.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_USonicMovementComponent.OuterSingleton, Z_Construct_UClass_USonicMovementComponent_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_USonicMovementComponent.OuterSingleton;
	}
	template<> PROTOSONIC_API UClass* StaticClass<USonicMovementComponent>()
	{
		return USonicMovementComponent::StaticClass();
	}
	USonicMovementComponent::USonicMovementComponent(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(USonicMovementComponent);
	USonicMovementComponent::~USonicMovementComponent() {}
	struct Z_CompiledInDeferFile_FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicMovementComponent_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicMovementComponent_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_USonicMovementComponent, USonicMovementComponent::StaticClass, TEXT("USonicMovementComponent"), &Z_Registration_Info_UClass_USonicMovementComponent, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(USonicMovementComponent), 851636204U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicMovementComponent_h_846233393(TEXT("/Script/ProtoSonic"),
		Z_CompiledInDeferFile_FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicMovementComponent_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicMovementComponent_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
