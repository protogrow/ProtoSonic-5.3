// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ProtoSonic/SonicCharacter.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSonicCharacter() {}
// Cross Module References
	NINJACHARACTER_API UClass* Z_Construct_UClass_ANinjaCharacter();
	PROTOSONIC_API UClass* Z_Construct_UClass_ASonicCharacter();
	PROTOSONIC_API UClass* Z_Construct_UClass_ASonicCharacter_NoRegister();
	UPackage* Z_Construct_UPackage__Script_ProtoSonic();
// End Cross Module References
	void ASonicCharacter::StaticRegisterNativesASonicCharacter()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ASonicCharacter);
	UClass* Z_Construct_UClass_ASonicCharacter_NoRegister()
	{
		return ASonicCharacter::StaticClass();
	}
	struct Z_Construct_UClass_ASonicCharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASonicCharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ANinjaCharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_ProtoSonic,
	};
	static_assert(UE_ARRAY_COUNT(Z_Construct_UClass_ASonicCharacter_Statics::DependentSingletons) < 16);
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASonicCharacter_Statics::Class_MetaDataParams[] = {
#if !UE_BUILD_SHIPPING
		{ "Comment", "/**\n * \n */" },
#endif
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "SonicCharacter.h" },
		{ "ModuleRelativePath", "SonicCharacter.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASonicCharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASonicCharacter>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ASonicCharacter_Statics::ClassParams = {
		&ASonicCharacter::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(UE_ARRAY_COUNT(Z_Construct_UClass_ASonicCharacter_Statics::Class_MetaDataParams), Z_Construct_UClass_ASonicCharacter_Statics::Class_MetaDataParams)
	};
	UClass* Z_Construct_UClass_ASonicCharacter()
	{
		if (!Z_Registration_Info_UClass_ASonicCharacter.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ASonicCharacter.OuterSingleton, Z_Construct_UClass_ASonicCharacter_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ASonicCharacter.OuterSingleton;
	}
	template<> PROTOSONIC_API UClass* StaticClass<ASonicCharacter>()
	{
		return ASonicCharacter::StaticClass();
	}
	ASonicCharacter::ASonicCharacter(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASonicCharacter);
	ASonicCharacter::~ASonicCharacter() {}
	struct Z_CompiledInDeferFile_FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicCharacter_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicCharacter_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ASonicCharacter, ASonicCharacter::StaticClass, TEXT("ASonicCharacter"), &Z_Registration_Info_UClass_ASonicCharacter, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ASonicCharacter), 1847555886U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicCharacter_h_1449935563(TEXT("/Script/ProtoSonic"),
		Z_CompiledInDeferFile_FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicCharacter_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Users_mapur_OneDrive_Desktop_UE5Sonic_ProtoSonic_5_3_Source_ProtoSonic_SonicCharacter_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
