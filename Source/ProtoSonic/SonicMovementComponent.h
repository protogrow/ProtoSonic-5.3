// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "NinjaCharacterMovementComponent.h"
#include "SonicMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class PROTOSONIC_API USonicMovementComponent : public UNinjaCharacterMovementComponent
{
	GENERATED_BODY()
	
};
